package src.clickPackage;

import java.awt.Color;

import javax.swing.JOptionPane;
/*************************************************************************
* Class which communicates between the model and view.
*
* @author Ryan Korteway
* @author Carolyn Quigley
* @author Tyler Miller
* @version November 2016
*************************************************************************/
public final class ColorClickerController {
	
	/** Our model, which holds and manipulates data. */
	private ColorClickerModel ourModel;
	
	/*************************************************************************
	 * The public constructor for this class.
	 *************************************************************************/
	public ColorClickerController() {
		ourModel = new ColorClickerModel();
	}
	
	/*************************************************************************
	 * Sets logic for the game.
	 * @param style determines what logic to use for the game
	 * where true means round robin.
	 *************************************************************************/
	public void setStyle(final boolean style) {
		ourModel.setStyle(style);
	}

	/*************************************************************************
	 * Gets logic for the game.
	 * @return true if round robin.
	 *************************************************************************/
	public boolean getStyle() {
		return ourModel.getStyle();
	}

	/*************************************************************************
	 * Checks if player has won. If so, updates number of wins and displays
	 * message to player.
	 * @param theColors the array of Colors currently on the board.
	 * @return true if won.
	 *************************************************************************/
	public boolean winCheck(final Color[] theColors) {
		boolean win = ourModel.winCheck(theColors);
		if (win) {
			ourModel.incWins();
			JOptionPane.showMessageDialog(null, ourModel.getWinMessage() 
					   + " you have won: " + ourModel.getWinCount());
		}
		return win;
	}

	/*************************************************************************
	 * Sends the model the original state of the board to store.
	 * @param startColors an array of the original colors.
	 *************************************************************************/
	public void setOriginal(final Color[] startColors) {
		ourModel.setOriginal(startColors);
	}

	/*************************************************************************
	 * Retrieves the original state of the board.
	 * @return an array of the original colors.
	 *************************************************************************/
	public Color[] getOriginal() {
		return ourModel.getOriginal();
	}
	
}