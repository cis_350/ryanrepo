package src.clickPackage;

import java.awt.Color;

//import javax.swing.JButton;

/**
 * Our ColorClickerModel holding a variety of data for our view to use.
 * @author Ryan Korteway 
 * @author Tyler Miller 
 * @author Cari Quigley
 */
public class ColorClickerModel {
	/** winCount holds the number of times player has won. */
	private int winCount;
	
	/** holds the message displayed when player wins. */
	private String winMessage;
	
	/** holds the original layout of the board in case of reset. */
	private Color[] originalLayout; 
	
	//so no array of buttons, send an array of colors then.
	/** holds the style that the player selected. */
	private boolean ourStyle;
	
	/** Public constructor for the class. */
	public ColorClickerModel() {
		 winCount = 0;
         winMessage = "Congratulations! You won this round.";
         ourStyle = false;
		 originalLayout = null;
	}
	
	/*************************************************************************
	 * Checks the layout of the buttons and determines whether player has won.
	 * @param layout the array holding the layout of the board
	 * @return true if player has won
	 *************************************************************************/
	public final boolean winCheck(final Color[] layout) {
		int length = layout.length - 1;
		int center = length / 2;
		Color winner = layout[center];
		for (int i = 0; i <= length; i++) {  
			if (layout[i] != winner) {
				return false;
			}
		}
		return true;
	}
	
	/*************************************************************************
	 * Sets the original layout.
	 * @param original Contains the array representing the layout
	 * of the board.
	 *************************************************************************/
	public final void setOriginal(final Color[] original) {
		originalLayout = original;
	}
	
	/*************************************************************************
	 * Returns the original state of the board.
	 * @return originalLayout array containing original state of board.
	 *************************************************************************/
	public final Color[] getOriginal() {
		return originalLayout;
	}
	
	/*************************************************************************
	 * Increments the winCount when player wins.
	 *************************************************************************/
	public final void incWins() {
		winCount++;
	}
	
	/*************************************************************************
	 * Returns the number of wins.
	 * @return winCount the number of wins
	 *************************************************************************/
	public final int getWinCount() {
		return winCount;
	}
	
	/*************************************************************************
	 * Returns the winning message.
	 * @return winMessage, the message displayed when the player wins.
	 *************************************************************************/
	public final String getWinMessage() {
		return winMessage;
	}
	
	/*************************************************************************
	 * Returns the current logic style.
	 * @return ourStyle, the current logic style.
	 *************************************************************************/
	public final boolean getStyle() {
		return ourStyle;
	}
	
	/*************************************************************************
	 * Sets the current logic style.
	 * @param selectedStyle the current logic style.
	 *************************************************************************/
	public final void setStyle(final boolean selectedStyle) {
		ourStyle = selectedStyle;
	}
}
