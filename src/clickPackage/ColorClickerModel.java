package clickPackage;

import java.awt.Color;

//import javax.swing.JButton;

public class ColorClickerModel {
	private int winCount;
	
	private String winMessage;
	
	private Color[] originalLayout; 
	//so no array of buttons, send an array of colors then.
	
	private boolean ourStyle;
	
	public ColorClickerModel() {
		 winCount = 0;
         winMessage = "Congratulations! You won this round.";
         ourStyle = false;
		 originalLayout = null;
	}
	
	public boolean winCheck(Color[] layout) {
		int length = layout.length - 1;
		int center = length / 2;
		Color winner = layout[center];
		for (int i = 0; i < length; i++) {
			if (layout[i] != winner) {
				return false;
			}
		}
		return true;
	}
	
	public void setOriginal(Color[] original) {
		originalLayout = original;
	}
	
	public Color[] getOriginal() {
		return originalLayout;
	}
	
	public void incWins() {
		winCount++;
	}
	
	public int getWinCount() {
		return winCount;
	}
	
	public String getWinMessage() {
		return winMessage;
	}
	
	public boolean getStyle() {
		return ourStyle;
	}
}
