package clickPackage;

import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.util.Random;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class ColorClickerView implements ActionListener {

	private boolean isLarge = false;
	private boolean gameLogic = true; 
	//true is for round robin, false is for across the board.
	
	private final int colorCollectionArrayLength = 5;
	private final int colorCol3 = 3;
	private final int colorCol4 = 4;
	private final int theButtonsArrayLength = 9;
	private final int theGridLayout5 = 5;
	private final int theGridLayout3 = 3;
	
	private final int buttonArrayAssignment3 = 3;
	private final int buttonArrayAssignment4 = 4;
	private final int buttonArrayAssignment5 = 5;
	private final int buttonArrayAssignment6 = 6;
	private final int buttonArrayAssignment7 = 7;
	private final int buttonArrayAssignment8 = 8;
	
	
	//small buttons names will get a touch more confusing during
	//use of large game board mode.
	public JButton button00;
	public JButton button01;
	public JButton button02;
	public JButton button10;
	public JButton button11;
	public JButton button12;
	public JButton button20;
	public JButton button21;
	public JButton button22;
	public JButton[] theButtons;
	
	public Color[] colorCollection;
	
	public JFrame gameFrame;
	public JPanel gamePanel;
	public JMenuBar gameBar = new JMenuBar();
	public GridLayout gameGrid;
	
	public boolean didWin = false;
	
	/**
	 * The public constructor for this class.
	 */
	public ColorClickerView() {
		
		gameFrame = new JFrame("Color Clicker");
		gamePanel = new JPanel();
		JMenuBar menubar = new JMenuBar();
		JMenu menus = new JMenu("Menu");
		JMenu menu_logic = new JMenu("Logic Styles");
		JMenuItem menu_reset = new JMenuItem("Reset");
		JMenuItem menu_quit = new JMenuItem("Quit");
		JMenuItem new_logic1 = new JMenuItem("Round Robin");
		JMenuItem new_logic2 = new JMenuItem("Flip Flop");
		menus.add(menu_quit);
		menus.add(menu_reset);
		menu_logic.add(new_logic1);
		menu_logic.add(new_logic2);
		menubar.add(menus);
		menubar.add(menu_logic);
		gameFrame.setJMenuBar(menubar);
		
		
		
		colorCollection = new Color[colorCollectionArrayLength];
		colorCollection[0] = Color.red;
		colorCollection[1] = Color.blue;
		colorCollection[2] = Color.green;
		colorCollection[colorCol3] = Color.orange;
		colorCollection[colorCol4] = Color.black;
		
		if (isLarge) {
			//TODO initiate enough buttons for the large game board etc.
			//in this area reinitialize the theButtons array to be larger.
			gameGrid = new GridLayout(theGridLayout5, theGridLayout5);
			gamePanel.setLayout(gameGrid);
		} else {
			//finish initializing the buttons.
			gameGrid = new GridLayout(theGridLayout3, theGridLayout3);
			gamePanel.setLayout(gameGrid);
			
			button00 = new JButton("  00  ");
			button01 = new JButton("  01  ");
			button02 = new JButton("  02  ");
			button10 = new JButton("  10  ");
			button11 = new JButton("goal");
			button12 = new JButton("  12  ");
			button20 = new JButton("  20  ");
			button21 = new JButton("  21  ");
			button22 = new JButton("  22  ");
			
			button00.setOpaque(true);
			button01.setOpaque(true);
			button02.setOpaque(true);
			button10.setOpaque(true);
			button11.setOpaque(true);
			button12.setOpaque(true);
			button20.setOpaque(true);
			button21.setOpaque(true);
			button22.setOpaque(true);
			
			button00.addActionListener(this);
			button01.addActionListener(this);
			button02.addActionListener(this);
			button10.addActionListener(this);
			button11.addActionListener(this);
			button12.addActionListener(this);
			button20.addActionListener(this);
			button21.addActionListener(this);
			button22.addActionListener(this);
			
			gamePanel.add(button00);
			gamePanel.add(button01);
			gamePanel.add(button02);
			gamePanel.add(button10);
			gamePanel.add(button11);
			gamePanel.add(button12);
			gamePanel.add(button20);
			gamePanel.add(button21);
			gamePanel.add(button22);
			
			theButtons = new JButton[theButtonsArrayLength];
			
			theButtons[0] = button00; 
			theButtons[1] = button01; 
			theButtons[2] = button02; 
			theButtons[buttonArrayAssignment3] = button10; 
			theButtons[buttonArrayAssignment4] = button11; 
			theButtons[buttonArrayAssignment5] = button12; 
			theButtons[buttonArrayAssignment6] = button20; 
			theButtons[buttonArrayAssignment7] = button21; 
			theButtons[buttonArrayAssignment8] = button22;
			
			//next up is assigning the buttons to their columns and adding
			//the columns to the frame itself.
		}
		viewReset(theButtons);
		gameFrame.add(gamePanel);
		gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameFrame.pack();
		gameFrame.setVisible(true);
	}
	

	
	public final void boardSelection(final boolean selection) {
		isLarge = selection;
	}
	
	@Override
	public final void actionPerformed(final ActionEvent e) {
		// small if checks for reset and restart and quit and the 
		//logic switches before all of the rest
		//Larger if doing special clicks needed else 
		//do one of the logics for Release 2.
		if (gameLogic) { //true for round robin.
			if (e.getSource() == button00) {
					Color currentColor = button20.getForeground();
					for (int counter = 0; counter < colorCollection.length; 
							counter++) {
						if ((counter + 1) == colorCollection.length) {
							button20.setForeground(colorCollection[0]);
							break;
						} else if (currentColor == colorCollection[counter]) {
						   button20.setForeground(colorCollection[counter + 1]);
						   break;
						}
					}
				} else if (e.getSource() == button01) {
				Color currentColor = button10.getForeground();
				for (int counter = 0; counter < colorCollection.length;
						counter++) {
					if ((counter + 1) == colorCollection.length) {
						button10.setForeground(colorCollection[0]);
						break;
					} else if (currentColor == colorCollection[counter]) {
						button10.setForeground(colorCollection[counter + 1]);
						break;
					}
				}
			} else if (e.getSource() == button02) {
				Color currentColor = button00.getForeground();
				for (int counter = 0; counter < colorCollection.length;
						counter++) {
					if ((counter + 1) == colorCollection.length) {
						button00.setForeground(colorCollection[0]);
						break;
					} else if (currentColor == colorCollection[counter]) {
						button00.setForeground(colorCollection[counter + 1]);
						break;
					}
				}
			} else if (e.getSource() == button10) {
				Color currentColor = button21.getForeground();
				for (int counter = 0; counter < colorCollection.length;
						counter++) {
					if ((counter + 1) == colorCollection.length) {
						button21.setForeground(colorCollection[0]);
						break;
					} else if (currentColor == colorCollection[counter]) {
						button21.setForeground(colorCollection[counter + 1]);
						break;
					}
				}
				//no else if for button11 because it is the winning color
				//button, no actions should touch it.
				
			} else if( e.getSource() == button12) {
				Color currentColor = button01.getForeground();
				for(int counter = 0; counter < colorCollection.length; counter++) {
					if(counter+1 == colorCollection.length) {
						button01.setForeground(colorCollection[0]);
						break;
					}
					else if( currentColor == colorCollection[counter]) {
						button01.setForeground(colorCollection[counter+1]);
						break;
					}
				}
			} 
			else if( e.getSource() == button20) {
				Color currentColor = button22.getForeground();
				for(int counter = 0; counter < colorCollection.length; counter++) {
					if(counter+1 == colorCollection.length) {
						button22.setForeground(colorCollection[0]);
						break;
					} else if( currentColor == colorCollection[counter]) {
						button22.setForeground(colorCollection[counter+1]);
						break;
					}
				}
			}
			else if( e.getSource() == button21) {
				Color currentColor = button12.getForeground();
				for(int counter = 0; counter < colorCollection.length; counter++) {
					if(counter+1 == colorCollection.length) {
						button12.setForeground(colorCollection[0]);
						break;
					} else if( currentColor == colorCollection[counter]) {
						button12.setForeground(colorCollection[counter+1]);
						break;
					}
				}
			}
			else if( e.getSource() == button22) {
				Color currentColor = button02.getForeground();
				for(int counter = 0; counter < colorCollection.length; counter++) {
					if(counter+1 == colorCollection.length) {
						button02.setForeground(colorCollection[0]);
						break;
					} else if( currentColor == colorCollection[counter]) {
						button02.setForeground(colorCollection[counter+1]);
						break;
					}
				}
			}
		}
		else //false is across the board.
		{
			
		}
		//TODO winner check. going through the button's array 
		//and comparing the colors of each with the center button. going to be done in model.
		//didWin = model.winCheck(theButtons); //still need a global model thats interacted with via controller.
	}
	

	//button color reassignment method, use a random number generator to 
	//assign a color via the color array for each of the buttons.
	/*
	 * for loop through the buttons via the array, recalculate a random number from 0 through 4,
	 * whatever the random number is what color the button becomes thanks to a switch statement
	 * that is executed within the loop and controlled with the random number. 
	 */
	public void viewReset(JButton[] buttonsToChange){
		Random generator = new Random();
		int randomNumber = 0;
		for (int x = 0; x < buttonsToChange.length; x++) {
			randomNumber = generator.nextInt(5);
			//System.out.print("Color setup " + colorCollection[randomNumber].toString());
			buttonsToChange[x].setForeground(colorCollection[randomNumber]);
		}
		//and then here we would pass a copy of the array to the model for hanging onto.
	}
	
	//need a get method from model to get a copy of the button array sent to it, and then
	//use said returned array to reset the buttons back to their start positions from the models copy.
//	public void resetFromModel(){
//		ColorClickerModel model; //a global model variable needs to be initiated 
//		//in main and then passed to the view through the controller.
//		JButton[] backToBeginning = model.getOriginal();
//		Random generator = new Random();
//		int randomNumber = 0;
//		for(int x = 0; x < backToBeginning.length; x++){
//			randomNumber = generator.nextInt(5);
//			backToBeginning[x].setForeground(colorCollection[randomNumber]); 
//			//might need to be changing the global array of buttons, not this local name for them here.
//			//backToBeginning might need to be theButtons[], same with the normal viewReset method.
//		}
//		//and then here we would pass a copy of the array to the model for hanging onto.
//	}	
	
	public String toString(){
		return "hi";
	}
	
}