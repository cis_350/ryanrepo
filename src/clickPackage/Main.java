package clickPackage;

/**
 * The main class that initiates our game.
 * @author Ryan Korteway
 *
 */
public final class Main {

  public static void main(String[] args) {
    ColorClickerView viewer = new ColorClickerView();
    System.out.println(viewer.toString());
  }
}