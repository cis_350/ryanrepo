package src.clickPackage;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;

import java.awt.Color;


/*************************************************************************
* Class which tests the Color Clicker game.
*
* @author Ryan Korteway
* @author Carolyn Quigley
* @author Tyler Miller
* @version October 2016
*************************************************************************/
public class ColorClickerTest {
	/** Large number to increment wins to for testing purposes. */
	public static final int MANYWINS = 10000000;
	
	/** Full array length. */
	public static final int ARRAYLENGTH = 9;
	
	/** Center of array which contains goal color. */
	public static final int CENTER = 4;
	
	/** Random number of correct color buttons. */
	public static final int THREE = 3;
	
	/** No correct color buttons. */
	public static final int ZERO = 0;
	
	/** All correct buttons. */
	public static final int ALL = 9;
	
	/********************************************************************
	 * Tests that ColorClickerModel's getWinMessage method works correctly.
	 ********************************************************************/
	@Test
	public final void testGetMessage() {
        	ColorClickerModel p = new ColorClickerModel();
        	String message = p.getWinMessage();
        	assertEquals(message, "Congratulations! You won this round.");
	}
	/********************************************************************
	 * Tests that ColorClickerModel's getWinCount method works correctly
	 * when count equals zero.
	 ********************************************************************/
	@Test
	public final void testGetWinCount() {
        	ColorClickerModel p = new ColorClickerModel();
        	int count = p.getWinCount();
        	assertEquals(count, 0);
	}
	/********************************************************************
	 * Tests that ColorClickerModel's incWinCount method works correctly
	 * and that getWinCount also works with values other than zero.
	 ********************************************************************/
	@Test
	public final void testIncrementWinCount1() {
			ColorClickerModel p = new ColorClickerModel();
			p.incWins();
			int count = p.getWinCount();
			assertEquals(count, 1);
	}
	/********************************************************************
	 * Tests that ColorClickerModel's incWinCount method works correctly
	 * for larger numbers.
	 ********************************************************************/
	@Test
	public final void testIncrementWinCount2() {
			ColorClickerModel p = new ColorClickerModel();
			for (int i = 0; i < MANYWINS; i++) {
				p.incWins();
			}
			int count = p.getWinCount();
			assertEquals(count, MANYWINS);
	}
	/********************************************************************
	 * Tests that ColorClickerModel's getStyle method works correctly
	 * for default value of false.
	 ********************************************************************/
	@Test
	public final void testGetStyle() {
			ColorClickerModel p = new ColorClickerModel();
			assertFalse(p.getStyle());
	}
	/********************************************************************
	 * Tests that ColorClickerModel's setStyle method correctly changes
	 * the style, and that getStyle continues to work with the new value.
	 ********************************************************************/
	@Test
	public final void testSetStyleTrue() {
			ColorClickerModel p = new ColorClickerModel();
			p.setStyle(true);
			assertTrue(p.getStyle());
	}
	/********************************************************************
	 * Tests that ColorClickerModel's setStyle method works correctly
	 * when changing the value back to false.
	 ********************************************************************/
	@Test
	public final void testSetStyleFalse() {
			ColorClickerModel p = new ColorClickerModel();
			p.setStyle(true);
			p.setStyle(false);
			assertFalse(p.getStyle());
	}
	/********************************************************************
	 * Tests that ColorClickerModel's getOriginal method works correctly
	 * using default value of null from the constructor.
	 ********************************************************************/
	@Test
	public final void testGetOriginalNull() {
			ColorClickerModel p = new ColorClickerModel();
			assertArrayEquals(p.getOriginal(), null);
	}
	/********************************************************************
	 * Tests that ColorClickerModel's setOriginal method works correctly
	 * and that getOriginal will also work when returning data other
	 * than null.
	 ********************************************************************/
	@Test
	public final void testSetOriginal() {
			ColorClickerModel p = new ColorClickerModel();
			Color[] a = new Color[2];
			a[0] = Color.black;
			a[1] = Color.cyan;
			p.setOriginal(a);
			Color[] check = p.getOriginal();
			assertArrayEquals(check, a);
	}
	/********************************************************************
	 * Tests that ColorClickerModel's winCheck method works correctly
	 * when player has won.
	 ********************************************************************/
	@Test
	public final void testWinCheckYes() {
			ColorClickerModel p = new ColorClickerModel();
			Color[] a = new Color[ARRAYLENGTH];
			for (int i = 0; i < ARRAYLENGTH; i++) {
				a[i] = Color.red;
			}
			assertTrue(p.winCheck(a));
	}
	/********************************************************************
	 * Tests that ColorClickerModel's winCheck method works correctly
	 * when player has not won.
	 ********************************************************************/
	@Test
	public final void testWinCheckNo() {
			ColorClickerModel p = new ColorClickerModel();
			Color[] a = new Color[ARRAYLENGTH];
			for (int i = 0; i < ARRAYLENGTH; i++) {
				a[i] = Color.red;
			}
			a[1] = Color.green;
			assertFalse(p.winCheck(a));
	}
	/********************************************************************
	 * Tests that ColorClickerModel's winCheck method works correctly
	 * when player has not won, when all colors except the goal are the
	 * same.
	 ********************************************************************/
	@Test
	public final void testWinCheckNoGoal() {
			ColorClickerModel p = new ColorClickerModel();
			Color[] a = new Color[ARRAYLENGTH];
			for (int i = 0; i < ARRAYLENGTH; i++) {
				a[i] = Color.red;
			}
			a[CENTER] = Color.green;
			assertFalse(p.winCheck(a));
	}
}
