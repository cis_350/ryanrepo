package src.clickPackage;

import javax.swing.JOptionPane;

/**
 * The main class that initiates our game.
 * @author Ryan Korteway, Tyler Miller, Cari Quigley
 *
 */
public final class Main {

	/**
	 * The main method that initiates our game.
	 * @author Ryan Korteway
	 * @param args the args from the command line
	 */
  public static void main(final String[] args) {
	Boolean selection = false;
	int ourSelection = JOptionPane.showConfirmDialog(null, 
		"Would you like to play with a 5x5 or a 3x3 Board? \r\n"
		+ "Yes for 5x5, No for 3x3",
		"Welcome to Color Clicker", JOptionPane.YES_NO_OPTION);
//	System.out.println("you selected " + ourSelection);
    if (ourSelection == 0) {
    	selection = true;
    }
	ColorClickerView viewer = new ColorClickerView(selection);
    System.out.println(viewer.toString());
    Main makeMain = new Main();
    makeMain.toString();
  }
  
  /**
   * The main class constructor that makes checkstyle happy.
   * @author Ryan Korteway
   *
   */
  private Main() {
	  //the things we do to make check style happy.
  }
}