package src.clickPackage;

import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.Random;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/*************************************************************************
* Class which provides the view for the Color Clicker game.
*
* @author Ryan Korteway
* @author Carolyn Quigley
* @author Tyler Miller
* @version December 2016
*************************************************************************/
public class ColorClickerView implements ActionListener {
	
	/** Holds variable determining size of board. */
	private boolean isLarge = false;
		
	/** Determines logic style of game. */
	private boolean gameLogic = false; 
	//true is for round robin, false is for across the board.
	
	/** Holds length of array of colors. */
	private final int colorCollectionArrayLength = 5;
	/** Holds index of third color. */
	private final int colorCol3 = 3;
	/** Holds index of fourth color. */
	private final int colorCol4 = 4;
	/** Holds length of array of buttons for the small game board. */
	private final int theButtonsSmallArrayLength = 9;
	/** Holds the length of the array of butons for the large game board.*/
	private final int theButtonsLargeArrayLength = 25;
	/** Holds size of larger game board. */
	private final int theGridLayout5 = 5;
	/** Holds size of smaller game board. */
	private final int theGridLayout3 = 3;
	
	/** Holds button array index value for button 3. */
	private final int buttonArrayAssignment3 = 3; //make them static and caps
	/** Holds button array index value for button 4. */
	private final int buttonArrayAssignment4 = 4;
	/** Holds button array index value for button 5. */
	private final int buttonArrayAssignment5 = 5;
	/** Holds button array index value for button 6. */
	private final int buttonArrayAssignment6 = 6;
	/** Holds button array index value for button 7. */
	private final int buttonArrayAssignment7 = 7;
	/** Holds button array index value for button 8. */
	private final int buttonArrayAssignment8 = 8;
	
	/** Limit for random number generator. */
	private static final int RANDOMLIMIT = 5;
	/**How many special clicks user has vertical. */
	private int clickCountVert;
	/**How many special clicks user has horizontal. */
	private int clickCountHoriz;
	
	
	//small buttons names will get a touch more confusing during
	//use of large game board mode.
	/** Button at position 0, 0. */
	private JButton button00;
	/** Button at position 0, 1. */
	private JButton button01;
	/** Button at position 0, 2. */
	private JButton button02;
	/** Button at position 1, 0. */
	private JButton button10;
	/** Button at position 1, 1. */
	private JButton button11;
	/** Button at position 1, 2. */
	private JButton button12;
	/** Button at position 2, 0. */
	private JButton button20;
	/** Button at position 2, 1. */
	private JButton button21;
	/** Button at position 2, 2. */
	private JButton button22;
	
	/** The additional buttons being defined to be used 
	 * within the large game board.*/
	/** Button at Position 0, 3.*/
	private JButton button03;
	/** Button at Position 0, 4.*/
	private JButton button04;
	/** Button at Position 1, 3.*/
	private JButton button13;
	/** Button at Position 1, 4.*/
	private JButton button14;
	/** Button at Position 2, 3.*/
	private JButton button23;
	/** Button at Position 2, 4.*/
	private JButton button24;
	/** Button at Position 3, 0.*/
	private JButton button30;
	/** Button at Position 3, 1.*/
	private JButton button31;
	/** Button at Position 3, 2.*/
	private JButton button32;
	/** Button at Position 3, 3.*/
	private JButton button33;
	/** Button at Position 3, 4.*/
	private JButton button34;
	/** Button at Position 4, 0.*/
	private JButton button40;
	/** Button at Position 4, 1.*/
	private JButton button41;
	/** Button at Position 4, 2.*/
	private JButton button42;
	/** Button at Position 4, 3.*/
	private JButton button43;
	/** Button at Position 4, 4.*/
	private JButton button44;
	
	/** Array of buttons. */
	private static JButton[] theButtons;
	
	/** Array of possible colors. */
	private Color[] colorCollection;
	
	/** The array of colors that our buttons currently are.*/
	private Color[] theColors;

	/** JFrame for the game. */
	private JFrame gameFrame;
	/** Panel for the game. */
	private JPanel gamePanel;
	/** Layout for the game. */
	private GridLayout gameGrid;
	
	/** menu bar for the game. */
	private JMenuBar menubar;
	/** base menu for the standard menu items. */
	private JMenu menus;
	/** logic menu for the logic items. */
	private JMenu menuLogic;
	/** menu item for the special click moves. */
	private JMenu specialClicks;
	/** menu item for the reset. */
	private JMenuItem menuReset;
	/** menu item for the quit. */
	private JMenuItem menuQuit;
	/** menu item for the flip flop logic. */
	private JMenuItem newLogic1;
	/** menu item for the round robin logic. */
	private JMenuItem newLogic2;
	/** menu item for the Special click horizontal 3. */
	private JMenuItem specialClickHoriz;
	/** menu item for the special click 3 vertical. */
	private JMenuItem specialClickVert;
	
	/** Handles communication between model and view. */
	private ColorClickerController ourController;

	/** Holds button array index value 9. */
	private final int buttonArrayAssignment9 = 9;
	
	/** Holds button array index value 10. */
	private final int buttonArrayAssignment10 = 10;
	
	/** Holds button array index value 11. */
	private final int buttonArrayAssignment11 = 11;
	
	/** Holds button array index value 12. */
	private final int buttonArrayAssignment12 = 12;
	
	/** Holds button array index value 13. */
	private final int buttonArrayAssignment13 = 13;
	
	/** Holds button array index value 14. */
	private final int buttonArrayAssignment14 = 14;
	
	/** Holds button array index value 15. */
	private final int buttonArrayAssignment15 = 15;

	/** Holds button array index value 16. */
	private final int buttonArrayAssignment16 = 16;

	/** Holds button array index value 17. */
	private final int buttonArrayAssignment17  = 17;

	/** Holds button array index value 18. */
	private final int buttonArrayAssignment18  = 18;

	/** Holds button array index value 19. */
	private final int buttonArrayAssignment19  = 19;

	/** Holds button array index value 20. */
	private final int buttonArrayAssignment20 = 20;

	/** Holds button array index value 21. */
	private final int buttonArrayAssignment21 = 21;

	/** Holds button array index value 22. */
	private final int buttonArrayAssignment22  = 22;

	/** Holds button array index value 23. */
	private final int buttonArrayAssignment23  = 23;

	/** Holds button array index value 24. */
	private final int buttonArrayAssignment24  = 24;

	/** the Boolean flag to say if a specialty click has 
	 * been selected for use. */
	private boolean isSpecial;

	/** the boolean flag to specify which kind of specialty
	 *  click has been selected.*/
	private boolean clickKind;
	
	/*************************************************************************
	 * The public constructor for this class.
	 * @param ourInput the users choice on whether or not they want to play 
	 * with a 5x5 or a 3x3 game board.
	 *************************************************************************/
	public ColorClickerView(final Boolean ourInput) {		
		isLarge = ourInput;
		gameFrame = new JFrame("Color Clicker");
		gamePanel = new JPanel();
		menubar = new JMenuBar();
		menus = new JMenu("Menu");
		specialClicks = new JMenu("Special CLicks");
		menuLogic = new JMenu("Logic Styles");
		menuReset = new JMenuItem("Reset");
		menuQuit = new JMenuItem("Quit");
		newLogic1 = new JMenuItem("Round Robin");
		newLogic2 = new JMenuItem("Flip Flop");
		specialClickHoriz = new JMenuItem("3 Button Horizontal");
		specialClickVert = new JMenuItem("3 Button Vertical");		
		menuReset.addActionListener(this);
		menuQuit.addActionListener(this);
		newLogic1.addActionListener(this);
		newLogic2.addActionListener(this);
		specialClickHoriz.addActionListener(this);
		specialClickVert.addActionListener(this);		
		menus.add(menuQuit);
		menus.add(menuReset);
		menuLogic.add(newLogic1);
		menuLogic.add(newLogic2);
		specialClicks.add(specialClickHoriz);
		specialClicks.add(specialClickVert);
		menubar.add(menus);
		menubar.add(menuLogic);
		menubar.add(specialClicks);
		gameFrame.setJMenuBar(menubar);		
		colorCollection = new Color[colorCollectionArrayLength];
		colorCollection[0] = Color.red;
		colorCollection[1] = Color.blue;
		colorCollection[2] = Color.green;
		colorCollection[colorCol3] = Color.orange;
		colorCollection[colorCol4] = Color.black;
		if (isLarge) {
			gameGrid = new GridLayout(theGridLayout5, theGridLayout5);
			gamePanel.setLayout(gameGrid);
			
			button00 = new JButton("  00  ");
			button01 = new JButton("  01  ");
			button02 = new JButton("  02  ");
			button03 = new JButton("  03  ");
			button04 = new JButton("  04  ");
			
			button10 = new JButton("  10  ");
			button11 = new JButton("  11  ");
			button12 = new JButton("  12  ");
			button13 = new JButton("  13  ");
			button14 = new JButton("  14  ");
			
			button20 = new JButton("  20  ");
			button21 = new JButton("  21  ");
			button22 = new JButton("  goal  ");
			button23 = new JButton("  23  ");
			button24 = new JButton("  24  ");
			
			button30 = new JButton("  30  ");
			button31 = new JButton("  31  ");
			button32 = new JButton("  32  ");
			button33 = new JButton("  33  ");
			button34 = new JButton("  34  ");
			
			button40 = new JButton("  40  ");
			button41 = new JButton("  41  ");
			button42 = new JButton("  42  ");
			button43 = new JButton("  43  ");
			button44 = new JButton("  44  ");
			
			theButtons = new JButton[theButtonsLargeArrayLength];
			
			theButtons[0] = button00; 
			theButtons[1] = button01; 
			theButtons[2] = button02; 
			theButtons[buttonArrayAssignment3] = button03; 
			theButtons[buttonArrayAssignment4] = button04;
			
			theButtons[buttonArrayAssignment5] = button10; 
			theButtons[buttonArrayAssignment6] = button11; 
			theButtons[buttonArrayAssignment7] = button12; 
			theButtons[buttonArrayAssignment8] = button13;
			theButtons[buttonArrayAssignment9] = button14;
			
			theButtons[buttonArrayAssignment10] = button20;
			theButtons[buttonArrayAssignment11] = button21;
			theButtons[buttonArrayAssignment12] = button22;
			theButtons[buttonArrayAssignment13] = button23;
			theButtons[buttonArrayAssignment14] = button24;
			
			theButtons[buttonArrayAssignment15] = button30;
			theButtons[buttonArrayAssignment16] = button31;
			theButtons[buttonArrayAssignment17] = button32;
			theButtons[buttonArrayAssignment18] = button33;
			theButtons[buttonArrayAssignment19] = button34;
			
			theButtons[buttonArrayAssignment20] = button40;
			theButtons[buttonArrayAssignment21] = button41;
			theButtons[buttonArrayAssignment22] = button42;
			theButtons[buttonArrayAssignment23] = button43;
			theButtons[buttonArrayAssignment24] = button44;
		
			for (int x = 0; x < theButtons.length; x++) {
				theButtons[x].setOpaque(true);
				theButtons[x].addActionListener(this);
				gamePanel.add(theButtons[x]);
			}			
		} else {
			//finish initializing the buttons.
			gameGrid = new GridLayout(theGridLayout3, theGridLayout3);
			gamePanel.setLayout(gameGrid);
			button00 = new JButton("  00  ");
			button01 = new JButton("  01  ");
			button02 = new JButton("  02  ");
			button10 = new JButton("  10  ");
			button11 = new JButton("goal");
			button12 = new JButton("  12  ");
			button20 = new JButton("  20  ");
			button21 = new JButton("  21  ");
			button22 = new JButton("  22  ");
			
			theButtons = new JButton[theButtonsSmallArrayLength];
			
			theButtons[0] = button00; 
			theButtons[1] = button01; 
			theButtons[2] = button02; 
			theButtons[buttonArrayAssignment3] = button10; 
			theButtons[buttonArrayAssignment4] = button11; 
			theButtons[buttonArrayAssignment5] = button12; 
			theButtons[buttonArrayAssignment6] = button20; 
			theButtons[buttonArrayAssignment7] = button21; 
			theButtons[buttonArrayAssignment8] = button22;
			
			for (int x = 0; x < theButtons.length; x++) {
				theButtons[x].setOpaque(true);
				theButtons[x].addActionListener(this);
				gamePanel.add(theButtons[x]);
			}
			
		}
		ourController = new ColorClickerController();
		viewReset(theButtons);
		gameFrame.add(gamePanel);
		gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameFrame.pack();
		gameFrame.setVisible(true);
		theColors = new Color[theButtons.length];
	}
	
	/*************************************************************************
	 * Sets the size of the board to what the player specifies.
	 * @param selection Contains boolean value which indicates if
	 * the game board is large.
	 *************************************************************************/
	public final void boardSelection(final boolean selection) {
		isLarge = selection;
	}
	
	/*************************************************************************
	 * Sets a random number of special clicks for vertical clicks.
	 * @return clickCountVert returns the amount of clicks.
	 *************************************************************************/
	public final int setSpecialClickCountVert() { 
		Random rand = new Random();
		clickCountVert = rand.nextInt(RANDOMLIMIT); 
		//TODO remove the random limit cubed
		JOptionPane.showMessageDialog(null, "Number of vertical "
				+ "clicks for this level: " + clickCountVert);
		return clickCountVert;	
	}

	/*************************************************************************
	 * sets a random number of special clicks for vertical clicks.
	 * @return clickCountHoriz returns the amount of clicks.
	 *************************************************************************/

	public final int setSpecialClickCountHoriz() {
		Random rand = new Random();
		clickCountHoriz = rand.nextInt(RANDOMLIMIT);
		//TODO remove the random limit cubed
		JOptionPane.showMessageDialog(null, "Number of horizontal "
				+ "clicks for this level: " + clickCountHoriz);
		
		return clickCountHoriz;
	}
	
	@Override
	public final void actionPerformed(final ActionEvent e) {
		// small if checks for reset and restart and quit and the 
		//logic switches before all of the rest
		//Larger if doing special clicks needed else 
		//do one of the logics for Release 2.
		if (e.getSource() == menuQuit) {
			System.exit(0);
		}
			
		if (e.getSource() == menuReset) {
			startViewReset(); //needs to be with the original colors
		}
		
		if (e.getSource() == newLogic1) { //round robin pressed
			ourController.setStyle(true);
			gameLogic = ourController.getStyle();
		}
		
		if (e.getSource() == newLogic2) { //flip flop pressed.
			ourController.setStyle(false);
			gameLogic = ourController.getStyle();
		}
		
		//TODO the specialty clicks that should be the start of this 
		//if/else if/else block, the else if being the call to the 
		//largeGameboardReactor, the if is going to be the checking
		//for specialty clicks being done and a special method call
		//will be needed for that as well, two kinds of clicks, 
		//vertical 3, horizontal 3, if a choice of click has been
		//made then we decrement the available clicks by 1, set
		//the specialty click flag to be hot, and then pass into the
		//specialty clicks method our game logic, board size, and 
		//event source as well as specialty click variety.
		if (isSpecial) {
			specialClickHandler(clickKind, isLarge, e.getSource());
			isSpecial = false;
		} else if (isLarge) {
			largeGameboardReactor(gameLogic, e.getSource());
		} else {
			if (gameLogic) { //true for round robin.
			if (e.getSource() == button00) {
				changeButtons(button20);
			} else if (e.getSource() == button01) {
				changeButtons(button10);
			} else if (e.getSource() == button02) {
				changeButtons(button00);
			} else if (e.getSource() == button10) {
				changeButtons(button21);
				//no else if for button11 because it is the winning color
				//button, no actions should touch it.
			} else if (e.getSource() == button12) {
				changeButtons(button01);
			} else if (e.getSource() == button20) {
				changeButtons(button22);
			} else if (e.getSource() == button21) {
				changeButtons(button12);
			} else if (e.getSource() == button22) {
				changeButtons(button02);
			}
		} else { //else false logic and thus around the board diagonally.
			if (e.getSource() == button00) {
				changeButtons(button22);
			} else if (e.getSource() == button01) {
				changeButtons(button10);
			} else if (e.getSource() == button02) {
				changeButtons(button20);
			} else if (e.getSource() == button10) {
				changeButtons(button01);
			//no else if for button11 because it is the winning color
			//button, no actions should touch it.
			} else if (e.getSource() == button12) {
				changeButtons(button21);
			} else if (e.getSource() == button20) {
				changeButtons(button02);
			} else if (e.getSource() == button21) {
				changeButtons(button12);
			} else if (e.getSource() == button22) {
				changeButtons(button00);
			}
		}
	}
		
		currentColors(theButtons);
		if (ourController.winCheck(theColors)) {
			viewReset(theButtons);
		}
		
		/**uses a special click - 3 vertical buttons. */
		if (e.getSource() == specialClickVert) {
			if (clickCountVert >= 1) {
				JOptionPane.showMessageDialog(null, 
						"Vertical Clicks: " + clickCountVert);
				clickCountVert--;
				isSpecial = true;
				clickKind = true;
			} else {
				JOptionPane.showMessageDialog(null, 
						"Not Enough Special CLicks Left");
				
			}
			
		}
		
		/**uses a special click - 3 horizontal buttons. */
		if (e.getSource() == specialClickHoriz) { 
			if (clickCountHoriz >= 1) {
				JOptionPane.showMessageDialog(null, 
						"Horizontal Clicks: " + clickCountHoriz);
				clickCountHoriz--;
				isSpecial = true;
				clickKind = false;
			} else {
				JOptionPane.showMessageDialog(null, 
						"Not Enough Special CLicks Left");
				}
			}
		
 
	}
	
	/*************************************************************************
	* A separate method to handle all the button color changes for the large 
	* game board.
	* @param theLogic the boolean operator saying if we are using the 
	* roundRobin or flip flop logic. 
	* @param event the button object that was pushed.
	*************************************************************************/
	private void largeGameboardReactor(final boolean theLogic, 
			final Object event) {
		if (theLogic) { //true for round robin.
			if (event == button00) {
				changeButtons(button40);
			} else if (event == button01) {
				changeButtons(button10);
			} else if (event == button02) {
				changeButtons(button20);
			} else if (event == button03) {
				changeButtons(button01);
			} else if (event == button04) {
				changeButtons(button00); //end of first row
			} else if (event == button10) {
				changeButtons(button30);
			} else if (event == button11) {
				changeButtons(button31);
			} else if (event == button12) {
				changeButtons(button21);
			} /**/ else if (event == button13) {
				changeButtons(button11);
			} else if (event == button14) {
				changeButtons(button03); //end of second row.
			} else if (event == button20) {
				changeButtons(button42);
			} else if (event == button21) {
				changeButtons(button32);
			} else if (event == button23) {
				changeButtons(button12);
			} else if (event == button24) {
				changeButtons(button02); //end of third row
			} else if (event == button30) {
				changeButtons(button41);
			} /**/ else if (event == button31) {
				changeButtons(button33);
			} else if (event == button32) {
				changeButtons(button23);
			} else if (event == button33) {
				changeButtons(button13);
			} else if (event == button34) {
				changeButtons(button14); //end of the forth row
			} else if (event == button40) {
				changeButtons(button44);
			} else if (event == button41) {
				changeButtons(button43);
			} else if (event == button42) {
				changeButtons(button24);
			} /**/ else if (event == button43) {
				changeButtons(button34);
			} else if (event == button44) {
				changeButtons(button04);
			} 
		} else { //else false logic and thus around the board diagonally.
			
			if (event == button00) {
				changeButtons(button44);
			} else if (event == button01) {
				changeButtons(button10);
			} else if (event == button02) {
				changeButtons(button20);
			} else if (event == button03) {
				changeButtons(button30);
			} else if (event == button04) {
				changeButtons(button40); //end of first row
			} else if (event == button10) {
				changeButtons(button01);
			} else if (event == button11) {
				changeButtons(button33);
			} else if (event == button12) {
				changeButtons(button21);
			} /**/ else if (event == button13) {
				changeButtons(button31);
			} else if (event == button14) {
				changeButtons(button41); //end of second row.
			} else if (event == button20) {
				changeButtons(button02);
			} else if (event == button21) {
				changeButtons(button12);
			} else if (event == button23) {
				changeButtons(button32);
			} else if (event == button24) {
				changeButtons(button42); //end of third row
			} else if (event == button30) {
				changeButtons(button03);
			} /**/ else if (event == button31) {
				changeButtons(button13);
			} else if (event == button32) {
				changeButtons(button23);
			} else if (event == button33) {
				changeButtons(button11);
			} else if (event == button34) {
				changeButtons(button43); //end of the forth row
			} else if (event == button40) {
				changeButtons(button04);
			} else if (event == button41) {
				changeButtons(button14);
			} else if (event == button42) {
				changeButtons(button24);
			} /**/ else if (event == button43) {
				changeButtons(button34);
			} else if (event == button44) {
				changeButtons(button00);
			}
		}
	}
	
	/*************************************************************************
	* Changes several buttons given which button was clicked and which 
	* specialty click version was selected to be used by the player.
	* color. Horizontal specialty clicks are handled in their own separate 
	* method to keep check style happy.
	* @param localClickKind The boolean flag saying if we are doing a vertical 
	* specialty click with a true value, or a horizontal click with a false 
	* value
	* @param localIsLarge the boolean flag that specifies if we are playing 
	* on the 5x5 game board or the 3x3 game board.
	* @param ourButton the button that was clicked with the specialty click 
	* activated
	*************************************************************************/
	public final void specialClickHandler(final boolean localClickKind, 
					final boolean localIsLarge, final Object ourButton) {
			if (localClickKind) {
					//true for vertical, 
					if (localIsLarge) {
							if (ourButton == button00) {
									changeButtons(button00);
									changeButtons(button10);
							} else if (ourButton == button01) {
									changeButtons(button01);
									changeButtons(button11);
							} else if (ourButton == button02) {
									changeButtons(button02);
									changeButtons(button12);
							} else if (ourButton == button03) {
									changeButtons(button03);
									changeButtons(button13);
							} else if (ourButton == button04) {
									changeButtons(button04);
									changeButtons(button14);
							} else if (ourButton == button10) {
									changeButtons(button00);
									changeButtons(button10);
									changeButtons(button20);
							} else if (ourButton == button11) {
									changeButtons(button01);
									changeButtons(button11);
									changeButtons(button21);
							} else if (ourButton == button12) {
									changeButtons(button02);
									changeButtons(button12);
									//should have third item but then we would be
									//changing the goal button.
							} else if (ourButton == button13) {
									changeButtons(button03);
									changeButtons(button13);
									changeButtons(button23);
							} else if (ourButton == button14) {
									changeButtons(button04);
									changeButtons(button14);
									changeButtons(button24);
							} else if (ourButton == button20) {
									changeButtons(button10);
									changeButtons(button20);
									changeButtons(button30);
							} else if (ourButton == button21) {
									changeButtons(button11);
									changeButtons(button21);
									changeButtons(button31);
							} else if (ourButton == button23) {
									changeButtons(button13);
									changeButtons(button23);
									changeButtons(button33);
							} else if (ourButton == button24) {
									changeButtons(button14);
									changeButtons(button24);
									changeButtons(button34);
							} else if (ourButton == button30) {
									changeButtons(button20);
									changeButtons(button30);
									changeButtons(button40);
							} else if (ourButton == button31) {
									changeButtons(button21);
									changeButtons(button31);
									changeButtons(button41);
							} else if (ourButton == button32) {
									changeButtons(button32);
									changeButtons(button42);
							} else if (ourButton == button33) {
									changeButtons(button23);
									changeButtons(button33);
									changeButtons(button43);
							} else if (ourButton == button34) {
									changeButtons(button24);
									changeButtons(button34);
									changeButtons(button44);
							} else if (ourButton == button40) {
									changeButtons(button30);
									changeButtons(button40);
							} else if (ourButton == button41) {
									changeButtons(button31);
									changeButtons(button41);
							} else if (ourButton == button42) {
									changeButtons(button32);
									changeButtons(button42);
							} else if (ourButton == button43) {
									changeButtons(button33);
									changeButtons(button43);
							} else if (ourButton == button44) {
									changeButtons(button34);
									changeButtons(button44);
							} else if (ourButton == button22) {
									JOptionPane.showMessageDialog(null, 
							"No special clicking the goal button.");
							}
					} else { //small game board vertical
							if (ourButton == button00) {
									changeButtons(button00);
									changeButtons(button10);
							} else if (ourButton == button01) {
									changeButtons(button01);
							} else if (ourButton == button02) {
									changeButtons(button02);
									changeButtons(button12);
							} else if (ourButton == button10) {
									changeButtons(button00);
									changeButtons(button10);
									changeButtons(button20);
							} else if (ourButton == button12) {
									changeButtons(button02);
									changeButtons(button12);
									changeButtons(button22);
							} else if (ourButton == button20) {
									changeButtons(button10);
									changeButtons(button20);
							} else if (ourButton == button21) {
									changeButtons(button21);
							} else if (ourButton == button22) {
									changeButtons(button12);
									changeButtons(button22);
							} else if (ourButton == button11) {
									JOptionPane.showMessageDialog(null, 
													"No special clicking the goal button.");
							}
					}
			} else {
					//else we are doing a horizontal click.
					horizontalSpecialtyClickHandler(localIsLarge, ourButton);
			}
	}
	
	/*************************************************************************
	* Changes several buttons at once given which button was clicked and the
	* fact that we are changing the buttons with the horizontal logic.
	* @param localIsLarge the boolean flag that specifies if we are playing 
	* on the 5x5 game board or the 3x3 game board.
	* @param ourButton the button that was clicked with the specialty click 
	* activated
	*************************************************************************/
	public final void horizontalSpecialtyClickHandler(final boolean 
			localIsLarge, final Object ourButton) {
		//horizontal large
		if (localIsLarge) {
			if (ourButton == button00) {
				changeButtons(button00);
				changeButtons(button01);
			} else if (ourButton == button01) {
				changeButtons(button00);
				changeButtons(button01);
				changeButtons(button02);
			} else if (ourButton == button02) {
				changeButtons(button01);
				changeButtons(button02);
				changeButtons(button03);
			} else if (ourButton == button03) {
				changeButtons(button02);
				changeButtons(button03);
				changeButtons(button04);
			} else if (ourButton == button04) {
				changeButtons(button03);
				changeButtons(button04);
			} else if (ourButton == button10) {
				changeButtons(button10);
				changeButtons(button11);
			} else if (ourButton == button11) {
				changeButtons(button10);
				changeButtons(button11);
				changeButtons(button12);
			} else if (ourButton == button12) {
				changeButtons(button11);
				changeButtons(button12);
				changeButtons(button13);
			} else if (ourButton == button13) {
				changeButtons(button12);
				changeButtons(button13);
				changeButtons(button14);
			} else if (ourButton == button14) {
				changeButtons(button13);
				changeButtons(button14);
			} else if (ourButton == button20) {
				changeButtons(button20);
				changeButtons(button21);
			} else if (ourButton == button21) {
				changeButtons(button20);
				changeButtons(button21);
			} else if (ourButton == button23) {
				changeButtons(button23);
				changeButtons(button24);
			} else if (ourButton == button24) {
				changeButtons(button23);
				changeButtons(button24);
			} else if (ourButton == button30) {
				changeButtons(button30);
				changeButtons(button31);
			} else if (ourButton == button31) {
				changeButtons(button30);
				changeButtons(button31);
				changeButtons(button32);
			} else if (ourButton == button32) {
				changeButtons(button31);
				changeButtons(button32);
				changeButtons(button33);
			} else if (ourButton == button33) {
				changeButtons(button32);
				changeButtons(button33);
				changeButtons(button34);
			} else if (ourButton == button34) {
				changeButtons(button33);
				changeButtons(button34);
			} else if (ourButton == button40) {
				changeButtons(button40);
				changeButtons(button41);
			} else if (ourButton == button41) {
				changeButtons(button40);
				changeButtons(button41);
				changeButtons(button42);
			} else if (ourButton == button42) {
				changeButtons(button41);
				changeButtons(button42);
				changeButtons(button43);
			} else if (ourButton == button43) {
				changeButtons(button42);
				changeButtons(button43);
				changeButtons(button44);
			} else if (ourButton == button44) {
				changeButtons(button43);
				changeButtons(button44);
			} else if (ourButton == button22){
				JOptionPane.showMessageDialog(null, 
						"No special clicking the goal button.");
			}
		} else {
			//horizontal small.
			if (ourButton == button00) {
				changeButtons(button00);
				changeButtons(button01);
			} else if (ourButton == button01) {
				changeButtons(button00);
				changeButtons(button01);
				changeButtons(button02);
			} else if (ourButton == button02) {
				changeButtons(button01);
				changeButtons(button02);
			} else if (ourButton == button10) {
				changeButtons(button10);
			} else if (ourButton == button12) {
				changeButtons(button12);
			} else if (ourButton == button20) {
				changeButtons(button20);
				changeButtons(button21);
			} else if (ourButton == button21) {
				changeButtons(button20);
				changeButtons(button21);
				changeButtons(button22);
			} else if (ourButton == button22) {
				changeButtons(button21);
				changeButtons(button22);
			} else if (ourButton == button11) {
				JOptionPane.showMessageDialog(null, 
						"No special clicking the goal button.");
			}
		}
		
	}
	
	/*************************************************************************
	* Changes the color of the given button by going through the array of
	* color.
	* @param change The JButton to change the color of
	*************************************************************************/
	private void changeButtons(final JButton change) {
		Color currentColor = change.getForeground();
		for (int counter = 0; counter < colorCollection.length;
				counter++) {
			if ((counter + 1) == colorCollection.length) {
				change.setForeground(colorCollection[0]);
				break;
			} else if (currentColor == colorCollection[counter]) {
				change.setForeground(colorCollection[counter + 1]);
				break;
			}
		}
	}

	 /*************************************************************************
	 * Creates new game board by randomly generating a color 
	 * for all of the buttons.
	 * @param buttonsToChange 
	 *************************************************************************/
	public final void viewReset(final JButton[] buttonsToChange) {
		Random generator = new Random();
		Color[] startColors = new Color[buttonsToChange.length];
		int randomNumber = 0;
		for (int x = 0; x < buttonsToChange.length; x++) {
			randomNumber = generator.nextInt(RANDOMLIMIT);
			buttonsToChange[x].setForeground(colorCollection[randomNumber]);
			startColors[x] = colorCollection[randomNumber];
		}
		ourController.setOriginal(startColors); 
		setSpecialClickCountVert();
		setSpecialClickCountHoriz();
	}
	
	//need a get method from model to get a copy of the button array sent to it,
	//and then use said returned array to reset the buttons back to their start 
	//positions from the models copy.
	/**
	 * The reset from the copy of the button colors that the model had.
	 */
	public final void startViewReset() {
		theColors = ourController.getOriginal();
		for (int x = 0; x < theButtons.length; x++) {
			theButtons[x].setForeground(theColors[x]); 
		}
	}	
	
	/*************************************************************************
	 * toString method for the class.
	 * @return String for class
	 *************************************************************************/
	public final String toString() {
		return "hi";
	}

	/*************************************************************************
	 *  Saves the current board layout.
	 * @param ourButtons The buttons having their current colors saved.
	 *************************************************************************/
	public final void currentColors(final JButton[] ourButtons) {
		for (int x = 0; x < ourButtons.length; x++) {
			theColors[x] = ourButtons[x].getForeground();
		}
	}
	
}
	